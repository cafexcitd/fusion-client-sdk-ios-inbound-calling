//
//  ViewController.h
//  Fusion Client iOS SDK App
//

#import <UIKit/UIKit.h>
#import <ACBClientSDK/ACBUC.h>

@interface ViewController : UIViewController <ACBClientCallDelegate, ACBClientPhoneDelegate, UIAlertViewDelegate>

// UI methods
-(IBAction)dial:(id)sender;
-(IBAction)hangup:(id)sender;
-(IBAction)toggleMedia:(id)sender;

// UI components
@property (weak, nonatomic) IBOutlet UIView *remote;
@property (weak, nonatomic) IBOutlet UIView *local;
@property (weak, nonatomic) IBOutlet UITextField *number;
@property (weak, nonatomic) IBOutlet UITextView *log;
@property (weak, nonatomic) IBOutlet UISwitch *audio;
@property (weak, nonatomic) IBOutlet UISwitch *video;

// reference to the ACBUC lib
@property (weak, nonatomic) ACBUC *acbuc;

@end
