//
//  ViewController.m
//  Fusion Client iOS SDK App
//

#import "ViewController.h"
#import "ConfigViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // assign acbuc object and init local video stream
    self.acbuc = [ConfigViewController getACBUC];
    self.acbuc.phone.previewView = self.local;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

////////////////////////////////////////////
//
// UI Methods

-(IBAction)dial:(id)sender
{
    [self logMessage: [NSString stringWithFormat:@"User dialed %@", self.number.text]];
    ACBClientCall *call = [self.acbuc.phone
                           createCallToAddress:self.number.text
                           audio:self.audio.on
                           video:self.video.on
                           delegate:self];
    
    call.videoView = self.remote;
}

-(IBAction)hangup:(id)sender
{
    [self logMessage:@"User hungup"];
    [(ACBClientCall*)self.acbuc.phone.currentCalls.lastObject end];
}

-(IBAction)toggleMedia:(id)sender
{
    [self logMessage:[NSString stringWithFormat:
                      @"User video: %d, voice: %d",
                      self.video.on, self.audio.on] ];
}

-(IBAction)userDoneEnteringText:(id)sender
{
    [sender resignFirstResponder];
}

-(void)logMessage:(NSString*)message
{
    self.log.text = [NSString stringWithFormat:@"%@\n%@", message, self.log.text];
    NSLog(@":::%@", message);
}

-(NSString*) statusToString: (ACBClientCallStatus)status
{
    NSArray *names = [NSArray arrayWithObjects:
                      @"ACBClientCallStatusSetup",
                      @"ACBClientCallStatusAlerting",
                      @"ACBClientCallStatusRinging",
                      @"ACBClientCallStatusMediaPending",
                      @"ACBClientCallStatusInCall",
                      @"ACBClientCallStatusTimedOut",
                      @"ACBClientCallStatusBusy",
                      @"ACBClientCallStatusNotFound",
                      @"ACBClientCallStatusError",
                      @"ACBClientCallStatusEnded", nil];
    return [names objectAtIndex:status];
}

////////////////////////////////////////////
//
// ACBClientPhoneDelegate methods

-(void) phone:(ACBClientPhone*)phone didReceiveCall:(ACBClientCall*)call
{
    [self logMessage:@"didReceiveCall"];
    
    // assign the delegate for the call
    call.delegate = self;
    
    // prompt the user to accept or reject the new call
    NSString *msg = [NSString stringWithFormat:@"Accept call from: %@", call.remoteAddress];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Incoming Call!" message:msg delegate:self cancelButtonTitle:@"Reject" otherButtonTitles:@"Accept", nil];
    [alert show];
}

- (void) phone:(ACBClientPhone*)phone didChangeCaptureSetting:(ACBVideoCaptureSetting*)settings forCamera:(AVCaptureDevicePosition)camera
{
    [self logMessage:@"didChangeCaptureSetting"];
}


////////////////////////////////////////////
//
// UIAlertViewDelegate methods

- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    ACBClientCall *call = self.acbuc.phone.currentCalls.lastObject;
    if (buttonIndex == 1)
    {
        // user clicked to accept call
        // loop through all calls in case the an old call is still hanging around
        call.videoView = self.remote;
        [call answerWithAudio:self.audio.on video:self.video.on];
        [self logMessage:@"User accepted call"];
    }
    else
    {
        // user clicked to reject call
        [call end];
        [self logMessage:@"User rejected call"];
    }
}


////////////////////////////////////////////
//
// ACBClientCallDelegate methods

-(void) call:(ACBClientCall *)call didChangeRemoteDisplayName:(NSString *)name
{
    [self logMessage:[NSString stringWithFormat: @"didChangeRemoteDisplayName: %@", name]];
}

-(void) call:(ACBClientCall *)call didChangeStatus:(ACBClientCallStatus)status
{
    NSString *statusString = [self statusToString:status];
    [self logMessage:[NSString stringWithFormat: @"didChangeStatus: %@", statusString]];
}

-(void) call:(ACBClientCall *)call didReceiveCallFailure:(NSString *)message
{
    [self logMessage:[NSString stringWithFormat:@"didReceiveCallFailure: %@", message]];
}

-(void) call:(ACBClientCall *)call didReceiveDialFailure:(NSString *)message
{
    [self logMessage:[NSString stringWithFormat:@"didReceiveDialFailure: %@", message]];
}

-(void) callDidAddLocalMediaStream:(ACBClientCall *)call
{
    [self logMessage:@"callDidAddLocalMediaStream"];
}

-(void) callDidAddRemoteMediaStream:(ACBClientCall *)call
{
    [self logMessage:@"callDidAddRemoteMediaStream"];
}

-(void) callDidReceiveMediaChangeRequest:(ACBClientCall *)call
{
    [self logMessage:@"callDidReceiveMediaChangeRequest"];
}

- (void) call:(ACBClientCall*)call didReportInboundQualityChange:(NSUInteger)inboundQuality
{
    [self logMessage:[NSString stringWithFormat:@"calldidReportInboundQualityChange: %d", inboundQuality]];
}


@end
